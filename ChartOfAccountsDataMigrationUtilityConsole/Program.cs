﻿using ChartOfAccountsDataMigrationUtilityConsole.MetadataClasses;
using CSOM_ListMaintenance;
using System;
using System.Collections.Generic;

namespace ChartOfAccountsDataMigrationUtilityConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("ChartOfAccountsDataMigrationUtilityConsole requires a single argument between 1-4");
                return;
            }

            GraphAPI_SharePointLib.SharePointUpdater graphAPI_SharePoint = new GraphAPI_SharePointLib.SharePointUpdater();
           // graphAPI_SharePoint.TestAppend().Wait();
            

            #region Fold
            // COA-Data.Serv@sandc.com|Sa-V"L3ypU
            #endregion
            string tenant = @"https://sandc4.sharepoint.com/sites/COA_Data";
            SharePointListMaintenance slm = new SharePointListMaintenance(tenant);
            //if (!slm.Login("COA-Data.Serv@sandc.com", out string errorMessage))
            // No longer works on SharePoint Online
            //if (!slm.Login("marc.temkin@sandc.com", out string errorMessage))
            //    Console.WriteLine(errorMessage);

            ListMaintenanceMgr mgr = new ListMaintenanceMgr(slm);

            int argChoice = GetArgChoice(args);
            if (argChoice == -1)
                return;

            // Always create the entities that map the view columns
            // the list metadata before any other step
            if (!mgr.CreateEntities(out string errorMessage))
            {
                Console.WriteLine(errorMessage);
                return;
            }

            switch (argChoice)
            {
                case 1:
                    if (!mgr.RemoveLists(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    break;
                case 2:
                    if (!mgr.CreateLists(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    break;
                case 3:
                    if (!mgr.FillTargetLists(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    else
                        Console.WriteLine("Data Load completed!");
                    break;

                case 4:
                    if (!mgr.BreakInheritanceOfLists(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    else
                        Console.WriteLine("Break Inheritance completed!");
                    break;
                case 5:
                    if (!mgr.RemoveUniquePermissions(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    else
                        Console.WriteLine("Remove Unique Permissions completed!");
                    break;

                case 6:
                    if (!mgr.AddUniquePermissions(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    else
                        Console.WriteLine("Add Unique Permissions completed!");
                    break;

                case 7:
                    if (!mgr.DisplayPermissions(out errorMessage))
                    { Console.WriteLine(errorMessage); }
                    else
                        Console.WriteLine("Display Permissions completed!");
                    break;

            }

        }

        private static int GetArgChoice(string[] args)
        {
            int argChoice = -1;
            if (!Int32.TryParse(args[0], out argChoice))
            {
                Console.WriteLine("ChartOfAccountsDataMigrationUtilityConsole requires a single argument between 1-4");
                return argChoice;
            }

            if (argChoice < 1 || argChoice > 7)
            {
                Console.WriteLine("ChartOfAccountsDataMigrationUtilityConsole requires a single argument between 1-3");
                return -1;
            }
            return argChoice;

        }


    }
}
