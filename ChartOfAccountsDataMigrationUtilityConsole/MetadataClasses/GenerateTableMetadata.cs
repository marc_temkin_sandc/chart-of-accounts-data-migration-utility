﻿using CommonChartOfAccountsMetadata.MetadataClasses;
using RetrieveDatabaseViews.ViewDefinitions;
using System.Collections.Generic;

namespace ChartOfAccountsDataMigrationUtilityConsole.MetadataClasses
{
    class GenerateTableMetadata
    {
        /// <summary>
        /// Holds Field Information for each View/Table
        /// </summary>
        private Dictionary<string, Dictionary<string, string>> ViewFieldDefs =
            new Dictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Initialize the Field information for each View needed by the 
        /// Chart Of Accounts data updating application
        /// </summary>
        public void Init() 
        {
            Dictionary<string, string> FieldDefs = 
                MapProps<AccountsByDeptView>.GenerateFieldInfo();

            // Same table structure for Non-US and US views
            ViewFieldDefs.Add("CoaAccountsbydeptview", FieldDefs);
            ViewFieldDefs.Add("CoaAccountsbydeptviewU", FieldDefs);
            ViewFieldDefs.Add("CoaAccountsviewu",
                MapProps<AccountsView>.GenerateFieldInfo());

            FieldDefs = MapProps<DepartmentView>.GenerateFieldInfo();
            ViewFieldDefs.Add("CoaDepartmentview", FieldDefs);
            ViewFieldDefs.Add("CoaDepartmentviewU", FieldDefs);

            FieldDefs = MapProps<DeptByAccountView>.GenerateFieldInfo();
            ViewFieldDefs.Add("CoaDeptbyaccountview", FieldDefs);
            ViewFieldDefs.Add("CoaDeptbyaccountviewU", FieldDefs);

            FieldDefs = MapProps<SearchView>.GenerateFieldInfo();
            ViewFieldDefs.Add("CoaSearchViewUS", FieldDefs);
            ViewFieldDefs.Add("CoaSearchViewNonUS", FieldDefs);

            FieldDefs = MapProps<AccountsByAccountType>.GenerateFieldInfo();
            ViewFieldDefs.Add("CoaAccountsByAccountTypeUS", FieldDefs);

        }

   

    }
}
