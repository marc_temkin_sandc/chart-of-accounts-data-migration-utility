﻿using CommonChartOfAccountsMetadata.MetadataClasses;
using CSOM_ListMaintenance;
using RetrieveDatabaseViews;
using RetrieveDatabaseViews.ViewDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace ChartOfAccountsDataMigrationUtilityConsole
{
    class ListMaintenanceMgr
    {
        private SharePointListMaintenance _sharePointListMaintenance;
        private List<CustomMetaDataForSharePointList> _SearchEntities;
        private List<CustomMetaDataForSharePointList> _DataEntities;
        GraphAPI_SharePointLib.SharePointUpdater graphAPI_SharePoint = new GraphAPI_SharePointLib.SharePointUpdater();


        public ListMaintenanceMgr(SharePointListMaintenance sharePointListMaintenance)
        {
            _sharePointListMaintenance = sharePointListMaintenance;
        }

        public bool CreateEntities(out string errorMessage)
        {
            errorMessage = String.Empty;
            _SearchEntities = new List<CustomMetaDataForSharePointList>();
            _DataEntities = new List<CustomMetaDataForSharePointList>();


            CustomMetaDataForSharePointList metaData =
            ListMetaData.DefineViewMetaData<AccountsByDeptViewUS>();
            _DataEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<AccountsByDeptViewNonUS>();
            _DataEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<DepartmentViewUS>();
            _DataEntities.Add(metaData);
            metaData = ListMetaData.DefineViewMetaData<DepartmentViewNonUS>();
            _DataEntities.Add(metaData);
            metaData = ListMetaData.DefineViewMetaData<DeptByAccountViewUS>();
            _DataEntities.Add(metaData);
            metaData = ListMetaData.DefineViewMetaData<DeptByAccountViewNonUS>();
            _DataEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<AccountsViewUS>();
            _DataEntities.Add(metaData);
            metaData = ListMetaData.DefineViewMetaData<AccountsViewNonUS>();
            _DataEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<vAccountsByAccountType>();
            _DataEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<vAccountsByAccountTypeFrg>();
            _DataEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<SearchViewUs>();
            _SearchEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<SearchViewUsWithAlias>();
            _SearchEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<SearchViewNonUs>();
            _SearchEntities.Add(metaData);

            metaData = ListMetaData.DefineViewMetaData<SearchViewNonUsWithAlias>();
            _SearchEntities.Add(metaData);



            return true;
        }

       

        #region Permission Methods
        delegate bool SharePointPermissionMntFunc(CustomMetaDataForSharePointList entry, out string errorMessage);
        private bool CallSharePointPermissionFunction(SharePointPermissionMntFunc func, out string errorMessage)
        {
            errorMessage = String.Empty;
            if (_sharePointListMaintenance == null)
            {
                errorMessage = "Reference to Share Point List Mnt object is not initialized!";
                return false;
            }

            foreach (var entity in _DataEntities)
            {
                if (!func(entity, out errorMessage))
                {
                    break;
                }

            }
            foreach (var entity in _SearchEntities)
            {
                if (!func(entity, out errorMessage))
                {
                    break;
                }
            }

            return true;

        }

        public bool DisplayPermissions(out string errorMessage)
        {
            errorMessage = String.Empty;
            return CallSharePointPermissionFunction(
                _sharePointListMaintenance.ListPermissions, out errorMessage);
        }
        public bool AddUniquePermissions(out string errorMessage)
        {

            errorMessage = String.Empty;
            return CallSharePointPermissionFunction(_sharePointListMaintenance.AddUniquePermissions, out errorMessage);
        }
        public bool RemoveUniquePermissions(out string errorMessage)
        {
            errorMessage = String.Empty;
            return CallSharePointPermissionFunction(_sharePointListMaintenance.RemoveUniquePermissions, out errorMessage);
        }
        public bool BreakInheritanceOfLists(out string errorMessage)
        {
            errorMessage = String.Empty;
            return CallSharePointPermissionFunction(_sharePointListMaintenance.BreakInheritance, out errorMessage);
        }
        #endregion Permission Methods

        #region ListMaintenance Methods
        public bool CreateLists(out string errorMessage)
        {
            errorMessage = String.Empty;
            if (_sharePointListMaintenance == null)
            {
                errorMessage = "Reference to Share Point List Mnt object is not initialized!";
                return false;
            }



            foreach (var entity in _DataEntities)
            {
                if (!_sharePointListMaintenance.CreateList(entity))
                {
                    break;
                }
            }

            foreach (var entity in _SearchEntities)
            {
                if (!_sharePointListMaintenance.CreateList(entity))
                {
                    break;
                }
            }

            return false;
        }
        public bool RemoveLists(out string errorMessage)
        {
            errorMessage = String.Empty;
            if (_sharePointListMaintenance == null)
            {
                errorMessage = "Reference to Share Point List Mnt object is not initialized!";
                return false;
            }
            bool success = true;
            foreach (var entity in _DataEntities)
            {

                if (graphAPI_SharePoint == null)
                    return false;


                 success &= graphAPI_SharePoint.DeleteRecords(entity).Result;

                //if (!_sharePointListMaintenance.RemoveList(entity, out errorMessage))
                //{
                //    break;
                //}

            }
            foreach (var entity in _SearchEntities)
            {
                success &= graphAPI_SharePoint.DeleteRecords(entity).Result;
                //if (!_sharePointListMaintenance.RemoveList(entity, out errorMessage))
                //{
                //    break;
                //}
            }

            return success;
        }

        internal bool FillTargetLists(out string errorMessage)
        {
            errorMessage = String.Empty;

            // .Where(tbl => tbl.baseClass.Name == "DepartmentView")
            using (ModelContext ctx = new ModelContext())
            {
                foreach (var dataentity in _DataEntities)
                    //.Where(ent => ent.className == "AccountsViewNonUS"))
                {

                    if (!FillDataList(ctx, dataentity, out errorMessage))
                        return false;
                }
               

                foreach (var entity in _SearchEntities)
                {
                    if (!FillSearchList(ctx, entity, out errorMessage))
                        return false;
                }

            }
            return true;
        }

        private bool FillDataList(ModelContext ctx, CustomMetaDataForSharePointList metaData, out string errorMessage)
        {
            errorMessage = String.Empty;
            List<string> jsonData = null;

            switch (metaData.baseClass.Name)
            {
                case nameof(AccountsByAccountType):

                    jsonData =
                    ctx.CoaAccountsByAccountType
                    .OrderBy(o => o.Account).ToList()
                    .Select(rec => JsonSerializer.Serialize(rec)).ToList();
                    break;

                case nameof(AccountsByAccountTypeFrg):

                    jsonData =
                    ctx.CoaAccountsByAccountTypeFrg
                    .OrderBy(o => o.Account).ToList()
                    .Select(rec => JsonSerializer.Serialize(rec)).ToList();

                    break;

                case nameof(AccountsByDeptView):

                    ICustomViewFilter<AccountsByDeptView> viewAD = null;
                    if (metaData.className == nameof(AccountsByDeptViewUS))
                        viewAD = new AccountsByDeptViewUS();
                    else
                        viewAD = new AccountsByDeptViewNonUS();

                    jsonData =
                    ctx.CoaAccountsByDeptViews.
                    Where(viewAD.whereFilter())
                    .OrderBy(o => o.AccountName).ThenBy(o => o.DeptName).ToList()
                    .Select(rec => JsonSerializer.Serialize(rec)).ToList();
                    break;

                case nameof(DeptByAccountView):

                    ICustomViewFilter<DeptByAccountView> viewDA = null;
                    if (metaData.className == nameof(DeptByAccountViewUS))
                        viewDA = new DeptByAccountViewUS();
                    else
                        viewDA = new DeptByAccountViewNonUS();

                    jsonData =
                    ctx.CoaDeptByAccountViews.
                    Where(viewDA.whereFilter())
                    .OrderBy(o => o.Account).ThenBy(o => o.DeptName).ToList()
                    .Select(rec => JsonSerializer.Serialize(rec)).ToList();

                    break;

                case nameof(AccountsView):
                    ICustomViewFilter<AccountsView> viewAV = null;
                    if (metaData.className == nameof(AccountsViewUS))
                        viewAV = new AccountsViewUS();
                    else
                        viewAV = new AccountsViewNonUS();

                    jsonData =
                    ctx.CoaAccountsViews.
                    Where(viewAV.whereFilter())
                    .OrderBy(o => o.Account).ToList()
                    .Select(rec => JsonSerializer.Serialize(rec)).ToList();
                    break;

                case nameof(DepartmentView):
                    ICustomViewFilter<DepartmentView> viewDV = null;
                    if (metaData.className == nameof(DepartmentViewUS))
                        viewDV = new DepartmentViewUS();
                    else
                        viewDV = new DepartmentViewNonUS();

                    jsonData =
                    ctx.CoaDepartmentViews.
                    Where(viewDV.whereFilter())
                    .OrderBy(o => o.Department).ToList()
                    .Select(rec => JsonSerializer.Serialize(rec)).ToList();
                    break;
            }

            if (graphAPI_SharePoint == null)
                return false;


            bool success = graphAPI_SharePoint.AppendRecords(jsonData, metaData).Result;


            //if (!_sharePointListMaintenance.FillList(jsonData, metaData,
            //   out errorMessage))
            //    return false;

            return success;
        }

        private bool FillSearchList(ModelContext ctx, CustomMetaDataForSharePointList metaData,
            out string errorMessage)
        {
            errorMessage = String.Empty;
            ICustomSearchFilter<SearchView> searchView = null;
            switch (metaData.className)
            {
                case "SearchViewUs":

                    searchView = new SearchViewUs();
                    break;
                case "SearchViewUsWithAlias":
                    searchView = new SearchViewUsWithAlias();
                    break;
                case "SearchViewNonUs":
                    searchView = new SearchViewNonUs();
                    break;
                case "SearchViewNonUsWithAlias":
                    searchView = new SearchViewNonUsWithAlias();
                    break;
            }

            Func<SearchView, bool> whClause = searchView.whereFilter();

            List<string> jsonData = ctx.CoaSearchView.Where(whClause)
            .OrderBy(o => o.AccountName).ThenBy(o => o.DeptName).ToList()
            .Select(rec => JsonSerializer.Serialize(rec)).ToList();

            if (graphAPI_SharePoint == null)
                return false;

            bool success = graphAPI_SharePoint.AppendRecords(jsonData, metaData).Result;

            //if (!_sharePointListMaintenance.FillList(jsonData, entity,
            //    out errorMessage))
            //    return false;

            return true;
        }

        #endregion ListMaintenance Methods

    }
}

