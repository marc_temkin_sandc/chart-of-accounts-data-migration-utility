﻿using CommonChartOfAccountsMetadata.MetadataClasses;
using Microsoft.SharePoint.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text.Json;
using PnP.Core;
using PnP.Framework;

namespace CSOM_ListMaintenance
{

    public class SharePointListMaintenance
    {

        #region Fields
        /// <summary>
        /// Credentials needed for the ClientContext instance
        /// </summary>
        private SharePointOnlineCredentials _credentials = null;

        /// <summary>
        /// The SharePoint site URI
        /// </summary>
        private string _tenant;

        /// <summary>
        /// The context needed to communicate with the SharePoint server
        /// </summary>
        private ClientContext _context;

        /// <summary>
        /// The max count before posting data to SharePoint
        /// </summary>
        private int _maxPostItems = 25;
        #endregion Fields


        #region Public Methods
        /// <summary>
        /// Constructor that initializes the _tenant field 
        /// </summary>
        /// <param name="tenant">The SharePoint URI</param>
        /// <param name="maxPostItems">The max number of records to post/put to the 
        /// SharePoint server in a single Execute</param>
        public SharePointListMaintenance(string tenant, int maxPostItems = 25)
        {
            this._tenant = tenant;
            this._maxPostItems = maxPostItems;
        }

        /// <summary>
        /// User log-in which in turn creates the Client Context.
        /// This method currently is interactive to prompt the user for a password.
        /// Once a service account is created then the interactive password will be removed.
        /// Important: the _credentials instance ONLY works in the .NET Framework 4.x.
        /// This will not work in .NET Standard or Core which requires an Azure AD account.
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="errorMessage">Reason for failure</param>
        /// <returns>Success</returns>
        public bool Login(string username, out string errorMessage)
        {
            errorMessage = String.Empty;
            Console.WriteLine("Enter pwd:");
            //            string password = Console.ReadLine();
            string password = "hX78^LeC*";
            var securePassword = new SecureString();
            foreach (var c in password)
                securePassword.AppendChar(c);

            AuthenticationManager authManager = new AuthenticationManager();

            try
            {
                _credentials = new SharePointOnlineCredentials(username, securePassword);
                _context = O365LogOn(new Uri(_tenant));
                if (_context == null)
                    return false;
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method used to create a new SharePoint list using 
        /// metadata, the parameter, to describe the list properties and 
        /// the column properties of the list.
        /// </summary>
        /// <param name="sharePointList">Metadata used to describe the list and it's columns</param>
        /// <returns>Success</returns>
        public bool CreateList(CustomMetaDataForSharePointList sharePointList)
        {

            if (_context == null)
                return false;

            ListCreationInformation listCreationInformation = new ListCreationInformation();
            listCreationInformation.Title = sharePointList.DisplayName;
            listCreationInformation.Description = $"List Display for View '{sharePointList.DisplayName}'";
            listCreationInformation.TemplateType = (int)ListTemplateType.GenericList;
            List list = _context.Web.Lists.Add(listCreationInformation);
            list.Update();
            _context.ExecuteQuery();

            // Add columns 
            foreach (ListColumnType colType in sharePointList.ColumnDefinitions.Where(attr => !attr.UseCalculatedValue))
            {
                DefineColumnType(list, colType);
            }
            _context.ExecuteQuery();

            //foreach (ListColumnType colType in sharePointList.ColumnDefinitions.Where(attr => attr.UseCalculatedValue))
            //{
            //    DefineCalculatedColumnType(list, colType);
            //}
            //_context.ExecuteQuery();



            return true;
        }



        private void DefineCalculatedColumnType(List list, ListColumnType colType)
        {

            string formula = "<Formula>=DEPARTMENT&amp; \"-\" &amp;ACCOUNT</Formula>"
           + "<FieldRefs>"
           + "<FieldRef Name='DEPARTMENT' />"
           + "<FieldRef Name='ACCOUNT' />"
           + "</FieldRefs>";

            // Required = 'TRUE' ReadOnly = 'TRUE' 
            string schemaCalculatedField = "<Field Type='Calculated' Name='Dept_Acct' StaticName='Dept_Acct' " +
   "DisplayName = 'DEPT_ACCT' ResultType = 'Text' > " + formula + " </Field> ";
            list.Fields.AddFieldAsXml(schemaCalculatedField, true, AddFieldOptions.DefaultValue);
            /*
                string descrip = colType.IsLookupOnly ?
                    $"{colType.Description} is a Lookup/Filter field only" : colType.Description;

                string fieldSchema = $"<Field DisplayName = '{colType.SharePointColumnName}' Description ='{descrip}' Name='{colType.SharePointColumnName}' Type='{colType.SharePointType}' />";

                Field f = list.Fields.AddFieldAsXml(fieldSchema, true, AddFieldOptions.DefaultValue);
                f.Update();

                if (colType is CalculatedListColumnType calcType)
                {
                    FieldCalculated fc = _context.CastTo<FieldCalculated>(f);

                    fc.OutputType = FieldType.Text;
                    fc.Formula = calcType.Expression;
                    fc.Update();

                }
            */
        }

        private static void DefineColumnType(List list, ListColumnType colType)
        {
            string fieldSchema = String.Empty;
            string descrip = colType.IsLookupOnly ?
                $"{colType.Description} is a Lookup/Filter field only" : colType.Description;

            if (colType is TextListColumnType)
                fieldSchema = $"<Field DisplayName = '{colType.SharePointColumnName}' Description ='{descrip}' Name='{colType.SharePointColumnName}' MaxLength = '{(colType as TextListColumnType).MaxLength}' Type='{colType.SharePointType}' />";
            else
                fieldSchema = $"<Field DisplayName = '{colType.SharePointColumnName}' Description ='{descrip}' Name='{colType.SharePointColumnName}' Type='{colType.SharePointType}' />";

            Field f = list.Fields.AddFieldAsXml(fieldSchema, true, AddFieldOptions.DefaultValue);
        }


        #region From CSharp Corner
        /// <summary>    
        /// This function gets the site/list/list item permission details. And return it by a dictonary.    
        /// <seealso cref="https://www.c-sharpcorner.com/article/get-sharepoint-permissions-programmatically-using-csom/"/>
        /// </summary>    
        /// <param name="clientContext">type ClientContext</param>    
        /// <param name="queryString">type IQueryable<RoleAssignment></param>    
        /// <returns>return type is Dictionary<string, string></returns>    
        private Dictionary<string, string> GetPermissionDetails(ClientContext clientContext, IQueryable<RoleAssignment> queryString)
        {
            IEnumerable roles = clientContext.LoadQuery(queryString);
            clientContext.ExecuteQuery();

            Dictionary<string, string> permisionDetails = new Dictionary<string, string>();
            foreach (RoleAssignment ra in roles)
            {
                var rdc = ra.RoleDefinitionBindings;
                string permission = string.Empty;
                foreach (var rdbc in rdc)
                {
                    permission += rdbc.Name.ToString() + ", ";
                }
                permisionDetails.Add(ra.Member.Title, permission);
            }
            return permisionDetails;
        }
        #endregion

        //Everyone except external users
        public bool ListPermissions(CustomMetaDataForSharePointList customMetaData, out string errorMessage)
        {
            errorMessage = "";

            if (_context == null)
            {
                errorMessage = "No context connection to SharePoint!";
                return false;
            }

            List list = _context.Web.Lists.GetByTitle(customMetaData.DisplayName);
            _context.Load(list);
            _context.ExecuteQuery();

            IQueryable<RoleAssignment> queryForList = list.RoleAssignments.Include(roleAsg => roleAsg.Member,
                                                                                   roleAsg => roleAsg.RoleDefinitionBindings.Include(roleDef => roleDef.Name));
            Dictionary<string, string> permissions = GetPermissionDetails(_context, queryForList);
            permissions.ToList().ForEach(kv =>
                Console.WriteLine(
                    $"List:{customMetaData.DisplayName}, User:{kv.Key}, Permission:{kv.Value}"));


            return true;

        }

        public bool AddUniquePermissions(CustomMetaDataForSharePointList customMetaData, out string errorMessage)
        {
            errorMessage = "";


            if (_context == null)
            {
                errorMessage = "No context connection to SharePoint!";
                return false;
            }

            // clientcontext.Web.Lists.GetById - This option also can be used to get the list using List GUID
            // This value is NOT List internal name
            List targetList = _context.Web.Lists.GetByTitle(customMetaData.DisplayName);

            // Load the list property(HasUniqueRoleAssignments)
            _context.Load(targetList, target => target.HasUniqueRoleAssignments);
            _context.ExecuteQuery();


            // This method will work only if the role inheritence is broken(list has unique role assignments) on the list
            if (targetList.HasUniqueRoleAssignments)
            {
                // Write group name to be added in the list
                // Everyone except external users
                Group group = _context.Web.SiteGroups.GetByName("Everyone except external users");
                RoleDefinitionBindingCollection roleDefCollection = new RoleDefinitionBindingCollection(_context);

                // Set the permission level of the group for this particular list
                RoleDefinition readDef = _context.Web.RoleDefinitions.GetByName("Read");
                roleDefCollection.Add(readDef);

                Principal userGroup = group;
                RoleAssignment roleAssign = targetList.RoleAssignments.Add(userGroup, roleDefCollection);

                _context.Load(roleAssign);
                roleAssign.Update();
                _context.ExecuteQuery();

            }
            else
            {
                Console.WriteLine("Target list does not have unique permission");
            }



            return true;
        }



        public bool RemoveUniquePermissions(CustomMetaDataForSharePointList customMetaData, out string errorMessage)
        {
            errorMessage = "";


            if (_context == null)
            {
                errorMessage = "No context connection to SharePoint!";
                return false;
            }

            // clientcontext.Web.Lists.GetById - This option also can be used to get the list using List GUID
            // This value is NOT List internal name
            List targetList = _context.Web.Lists.GetByTitle(customMetaData.DisplayName);

            // Load the list property(HasUniqueRoleAssignments)
            _context.Load(targetList, target => target.HasUniqueRoleAssignments);
            _context.Load(targetList.RoleAssignments);
            _context.ExecuteQuery();

            // This method will work only if the role inheritence is broken(list has unique role assignments) on the list
            if (targetList.HasUniqueRoleAssignments)
            {
                // Write all the group names to be removed over here with separator
                string GroupNameswithseparator = "Marc Temkin Learning Site Members;Marc Temkin Learning Site Visitors;Marc Temkin Learning Site Owners";
                string[] groupsToRemove = GroupNameswithseparator.Split(';').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

                foreach (string objectToRemove in groupsToRemove)
                {
                    if (targetList.RoleAssignments.Count() > 1)
                    {
                        targetList.RoleAssignments.Groups.RemoveByLoginName(objectToRemove.ToString());
                        _context.ExecuteQuery();
                    }
                }
            }
            else
            {
                Console.WriteLine("Target list does not have unique permission");
            }



            return true;
        }


        /// <summary>
        /// Remove List
        /// </summary>
        /// <param name="customMetaData"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool BreakInheritance(CustomMetaDataForSharePointList customMetaData, out string errorMessage)
        {
            errorMessage = String.Empty;
            if (_context == null)
            {
                errorMessage = "No context connection to SharePoint!";
                return false;
            }

            List targetList = _context.Web.Lists.GetByTitle(customMetaData.DisplayName);
            try
            {
                _context.Load(targetList);
                if (targetList == null)
                {
                    errorMessage = $"{customMetaData.DisplayName} does not exists as a SharePoint list.";
                    return false;
                }
            }
            catch (Exception e)
            {
                errorMessage = $"{customMetaData.DisplayName} ({e.Message}) does not exists as a SharePoint list.";
                return false;
            }

            try
            {
                targetList.BreakRoleInheritance(true, true);
                targetList.Update();
                _context.ExecuteQuery();
            }
            catch (Exception e)
            {
                errorMessage = $"Break Role Inheritance Operation for: {customMetaData.DisplayName} failed due to {e.Message}.";
                return false;
            }
            return true;
        }


        /// <summary>
        /// Remove List
        /// </summary>
        /// <param name="customMetaData"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public bool RemoveList(CustomMetaDataForSharePointList customMetaData, out string errorMessage)
        {
            string listname = customMetaData.DisplayName;
            errorMessage = String.Empty;
            if (_context == null)
            {
                errorMessage = "No context connection to SharePoint!";
                return false;
            }

            List targetList = _context.Web.Lists.GetByTitle(customMetaData.DisplayName);
            try
            {
                _context.Load(targetList);
                if (targetList == null)
                {
                    errorMessage = $"{customMetaData.DisplayName} does not exists as a SharePoint list.";
                    return false;
                }
            }
            catch (Exception e)
            {
                errorMessage = $"{customMetaData.DisplayName} ({e.Message}) does not exists as a SharePoint list.";
                return false;
            }

            try
            {
                //targetList.DeleteObject();
                //_context.ExecuteQuery();
                return ClearList(customMetaData.DisplayName, out errorMessage);
            }
            catch (Exception e)
            {
                errorMessage = $"Delete Operation for: {customMetaData.DisplayName} failed due to {e.Message}.";
                return false;
            }
            //return false;
        }

        /// <summary>
        /// Top-level method to delete all the items in a list.
        /// </summary>
        /// <param name="listName">The name identifier of the list</param>
        /// <param name="errorMessage">Formatted message in the event of failure</param>
        /// <returns></returns>
        public bool ClearList(string listName, out string errorMessage)
        {
            errorMessage = String.Empty;


            if (_context == null)
                return false;

            List targetList = _context.Web.Lists.GetByTitle(listName);
            if (targetList == null)
            {
                errorMessage = $"{listName} does not exists as a SharePoint list.";
                return false;
            }
            deleteAllFromList(_context, targetList);

            return true;
        }

        /// <summary>
        /// Fill the list with data with the contents of the List of json strings.
        /// The customMetaData is used to match the table column property name with 
        /// the list column name.
        /// </summary>
        /// <param name="JsonData">The view data serialized in JSON</param>
        /// <param name="customMetaData">The metadata used to match the property names with the List column names</param>
        /// <param name="errorMessage">Failure message</param>
        /// <returns>Success</returns>
        public bool FillList(List<string> JsonData,
            CustomMetaDataForSharePointList customMetaData, out string errorMessage)
        {
            errorMessage = String.Empty;

            if (JsonData == null)
            {
                errorMessage = $"No data for {customMetaData.DisplayName}!";
                return false;
            }

            if (_context == null)
            {
                errorMessage = "No context connection to SharePoint!";
                return false;
            }
            List targetList = _context.Web.Lists.GetByTitle(customMetaData.DisplayName);
            if (targetList == null)
            {
                errorMessage = $"{customMetaData.DisplayName} does not exists as a SharePoint list.";
                return false;
            }

            // Current record number
            int cnt = 1;
            // Total record count for the input
            int total = JsonData.Count;

            // Create a dictionary to map the property name, used in the JSON input,
            // with the List Column Name.
            // The value in the matching column updates the newItem record which is 
            // inserted into the list.

            Dictionary<string, string> mapNameTblToList =
            customMetaData.ColumnDefinitions.
            Select(rec => new { viewColName = rec.ViewColumnName, listColName = rec.SharePointColumnName }).ToDictionary
                (key => key.viewColName, val => val.listColName);

            // Reference below is to a parallel approach that could be used later
            // See: https://docs.microsoft.com/en-us/dotnet/api/system.collections.concurrent.partitioner-1?view=netframework-4.8

            // Iterate the input and create new records for the target list
            foreach (string item in JsonData)
            {
                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
                ListItem newItem = targetList.AddItem(itemCreateInfo);

                // Look up the current Json Property to find the matching List Column.
                // Update the newItem structure with this KeyValue Pair.
                foreach (var obj in JsonDocument.Parse(item).RootElement.EnumerateObject())
                {
                    JsonProperty jp = obj;

                    if (mapNameTblToList.ContainsKey(jp.Name))
                    {
                        string listColName = mapNameTblToList[jp.Name];
                        newItem[listColName] = jp.Value;
                        newItem.Update();
                    }

                }
                // Update SharePoint every maxItems record
                if (cnt % _maxPostItems == 0)
                {
                    Console.WriteLine($"Line Count Processed:{cnt}/{total}");
                    _context.ExecuteQuery();
                }
                cnt++;

            }
            _context.ExecuteQuery();

            return true;
        }
        #endregion Public Methods

        #region Private/Protected Methods

        /// <summary>
        /// Delete all of the records in a list
        /// Page the List in order to delete
        /// <see cref="https://www.sharepointgems.com/2020/10/delete-all-items-from-sharepoint-list-with-csom/"/>
        /// </summary>
        /// <param name="cc"></param>
        /// <param name="myList"></param>
        private void deleteAllFromList(ClientContext cc, List myList)
        {
            int queryLimit = 4000;
            int batchLimit = 100;
            bool moreItems = true;

            string viewXml = string.Format(@"
        <View>
            <Query><Where></Where></Query>
            <ViewFields>
                <FieldRef Name='ID' />
            </ViewFields>
            <RowLimit>{0}</RowLimit>
        </View>", queryLimit);
            var camlQuery = new CamlQuery();
            camlQuery.ViewXml = viewXml;

            while (moreItems)
            {
                ListItemCollection listItems = myList.GetItems(camlQuery); // CamlQuery.CreateAllItemsQuery());
                cc.Load(listItems,
                    eachItem => eachItem.Include(
                        item => item,
                        item => item["ID"]));
                cc.ExecuteQuery();

                var totalListItems = listItems.Count;
                if (totalListItems > 0)
                {
                    Console.WriteLine("Deleting {0} items", totalListItems);
                    for (var i = totalListItems - 1; i > -1; i--)
                    {
                        listItems[i].DeleteObject();
                        if (i % batchLimit == 0)
                            cc.ExecuteQuery();
                    }
                    cc.ExecuteQuery();
                }
                else
                {
                    moreItems = false;
                }
            }
            Console.WriteLine("Deletion complete.");
        }

        /// <summary>
        /// Retrieve the ClientContext provided
        /// the credentials are accepted
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private ClientContext O365LogOn(Uri url)
        {
            ClientContext clientContext = null;
            ClientContext ctx = null;

            try
            {
                clientContext = new ClientContext(url);
                clientContext.Credentials = _credentials;

                //Web web = clientContext.Web;
                //clientContext.Load(web, webSite => webSite.Title);

                clientContext.ExecuteQuery();
                ctx = clientContext;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (clientContext != null)
                    clientContext.Dispose();
            }
            return ctx;
        }

        /// <summary>
        /// Display the fields for debugging purposes
        /// </summary>
        /// <param name="targetList">The target list for the Fill, Delete, or other operations</param>
        private void DisplayFields(List targetList)
        {
            _context.Load(targetList.Fields);
            _context.ExecuteQuery();

            int j = 0;
            foreach (Field field in targetList.Fields)
            {
                Console.WriteLine($"{j++},{field.TypeDisplayName}, {field.Title}");
            }
        }


        #region ContentType Methods

        /// <summary>
        /// Unused here but save for where a custom content type needs to be created.
        /// </summary>
        /// <param name="contentTypeName">Name of the content type</param>
        /// <param name="description">It's purpose</param>
        /// <param name="context">The client context</param>
        private static void CreateContentType(string contentTypeName, string description,
            ClientContext context)
        {
            // Cache a reference to the content types collection for the site
            ContentTypeCollection contentTypes = context.Web.ContentTypes;

            // Load (retrieve) the content types collection
            context.Load(contentTypes);
            context.ExecuteQuery();

            ContentType contentType = null;
            // Already exists
            if (!contentTypes.Any(ct => ct.Name == contentTypeName))
                contentType = DefineContentType(contentTypeName, description, context, contentTypes);
            else
                contentType = contentTypes.First(ct => ct.Name == contentTypeName);
        }

        /// <summary>
        /// Unused now but called by CreateContentType
        /// </summary>
        /// <param name="name">Name of the type</param>
        /// <param name="description">It's purpose</param>
        /// <param name="context">The Client Context</param>
        /// <param name="contentTypes">The collection of Content Types in the site</param>
        /// <returns></returns>
        private static ContentType DefineContentType(string name, string description,
            ClientContext context, ContentTypeCollection contentTypes)
        {
            // Find the template used to define the new content type via inheritance
            ContentType parentType = contentTypes.Where(ct => ct.Name == "Item").FirstOrDefault();
            ContentTypeCreationInformation creationInformation = new ContentTypeCreationInformation();

            // Name, description and the group of the new content type
            creationInformation.Name = name;
            creationInformation.Description = description;
            creationInformation.Group = "Data Models Group";

            // Create the inheritance relationship 
            creationInformation.ParentContentType = parentType;

            // Add the new type to the collection of content types
            ContentType contentType = contentTypes.Add(creationInformation);
            context.ExecuteQuery();

            return contentType;
        }
        #endregion ContentType Methods
        #endregion Private/Protected Methods
    }
}

