﻿using Azure.Identity;
using CommonChartOfAccountsMetadata.MetadataClasses;
using Microsoft.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GraphAPI_SharePointLib
{
    public class SharePointUpdater
    {

        string[] scopes = new string[] { "user.read", "Sites.Read.All", "Sites.ReadWrite.All" }; //, 
        const string clientID = "5cce398a-0e10-4fd1-ae18-42305c087375";
        const string tenant = "sandc.com";
        const string redirectUrl = "msal5cce398a-0e10-4fd1-ae18-42305c087375://auth";
        string siteGuid = "1f066c80-d38b-4443-aea6-b04dd5e21ce2,a03ee96f-3553-47c2-8fdc-3e1d55cb6bac";
        GraphServiceClient graphClient = null;

        InteractiveBrowserCredentialOptions interactiveBrowserCredentialOptions;

        public SharePointUpdater()
        {
            interactiveBrowserCredentialOptions = new InteractiveBrowserCredentialOptions()
            {
                ClientId = clientID,
                RedirectUri = new System.Uri(redirectUrl)
            };
            InteractiveBrowserCredential interactiveBrowserCredential = new InteractiveBrowserCredential(interactiveBrowserCredentialOptions);

            graphClient = new GraphServiceClient(interactiveBrowserCredential, scopes);
        }

        public void Test()
        {
            SharepointLists().GetAwaiter().GetResult();
        }

        public async Task<bool> TestAppend()
        {
            string listNameSrc = "TestInsertFromGraph";
            var list = await graphClient.Sites[siteGuid].Lists[listNameSrc].Items.Request().GetAsync();

            var listItem = new ListItem
            {

                Fields = new FieldValueSet
                {

                    AdditionalData = new Dictionary<string, object>()
                            {
                                {"Title","Graph API 1"},
                                {"Description", "This is a test of the Graph API"},
                                {"OrderID", 101}
                            }
                }
            };

            var t = await graphClient.Sites[siteGuid].Lists[listNameSrc].Items.Request().AddResponseAsync(listItem);

            Console.WriteLine($"{listItem.Id}:{t.StatusCode}");
            return true;
        }

        private async Task SharepointLists()
        {
            var lists = await graphClient.Sites[siteGuid].Lists.Request().GetAsync();

            Regex regex = new Regex("^[A-Z_]+$");

            int i = 0;
            foreach (var list in lists)
            {
                try
                {
                    string listName = list.DisplayName;


                    var rqBldr = graphClient.Sites[siteGuid].Lists[listName];
                    if (rqBldr == null)
                    {
                        Console.WriteLine($"{listName} - Cannot create request builder");
                        continue;
                    }
                    if (rqBldr.Items == null)
                    {
                        Console.WriteLine($"{listName} - No items in this list");
                        continue;
                    }
                    var items = rqBldr.Items.Request().GetAsync().Result;
                    if (items == null)
                    {
                        Console.WriteLine($"{listName} - No records");
                        continue;
                    }
                    var recOne = rqBldr.Items[items.First().Id].Request().GetAsync().Result;

                    if (recOne == null)
                    {
                        Console.WriteLine($"{listName} - Cannot retrieve the first record");
                        continue;
                    }

                    if (recOne.Fields == null)
                    {
                        Console.WriteLine($"{listName} - Cannot retrieve the Fields record");
                        continue;
                    }
                    if (recOne.Fields.AdditionalData == null)
                    {
                        Console.WriteLine($"{listName} - Cannot retrieve the Additional Data");
                        continue;
                    }

                    var Fields = recOne.Fields.AdditionalData;

                    var Keys = Fields.Keys.Where(q => regex.IsMatch(q)).ToList();
                    foreach (var key in Keys)
                    {
                        var fld = Fields.First(q => q.Key == key);
                        var jd = (JsonElement)fld.Value;
                        Console.WriteLine($"{fld}->{jd}");

                    }

                    System.Console.WriteLine($"{i + 1}:{listName}");
                    Console.WriteLine($"Fields: {String.Join(",", Keys)}");
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error on ${list.Name} with error:{ex.Message}");
                }

                i++;
            }

        }
        public async Task<bool> DeleteRecords(CustomMetaDataForSharePointList customMetaData)
        {
            string listname = customMetaData.DisplayName;
            var list = await graphClient.Sites[siteGuid].Lists[listname].Items
             .Request()
             .Top(100)
             .GetAsync();

            int totalRecordCount = 0;
            Console.WriteLine($"Deleting records from: {listname}");
            while (list.Count > 0)
            {
                List<string> itemIds = list.Select(q => q.Id).ToList();
                totalRecordCount += list.Count;
                foreach (string itemId in itemIds)
                {
                    var gresp =
                    await graphClient.Sites[siteGuid].Lists[listname].Items[itemId]
                    .Request()
                    .Header("Prefer", "allowthrottleablequeries")
                    .DeleteResponseAsync();

                }

                Console.WriteLine($"{totalRecordCount}");
                if (list.NextPageRequest != null)
                {
                    list = await list.NextPageRequest.GetAsync();
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine($"{totalRecordCount}");
            return true;
        }


        public async Task<bool> AppendRecords(List<string> jsonData, CustomMetaDataForSharePointList customMetaData)
        {
            string errorMessage = String.Empty;
            int _maxPostItems = 100;



            string listNameTarg = customMetaData.DisplayName;

            if (jsonData == null)
            {
                errorMessage = $"No data for {listNameTarg}!";
                return false;
            }


            var rqBldr = graphClient.Sites[siteGuid].Lists[listNameTarg];
            if (rqBldr == null)
            {
                Console.WriteLine($"{listNameTarg} - Cannot create request builder");
                return false;
            }

            Console.WriteLine($"Processing to {listNameTarg}");

            // Current record number
            int cnt = 1;
            // Total record count for the input
            int total = jsonData.Count;

            // Create a dictionary to map the property name, used in the JSON input,
            // with the List Column Name.
            // The value in the matching column updates the newItem record which is 
            // inserted into the list.

            Dictionary<string, string> mapNameTblToList =
            customMetaData.ColumnDefinitions.
            Select(rec => new { viewColName = rec.ViewColumnName, listColName = rec.SharePointColumnName }).ToDictionary
                (key => key.viewColName, val => val.listColName);




            foreach (string item in jsonData)
            {

                Dictionary<string, object> curAdditionalData = new Dictionary<string, object>();


                // Look up the current Json Property to find the matching List Column.
                // Update the newItem structure with this KeyValue Pair.
                foreach (var obj in JsonDocument.Parse(item).RootElement.EnumerateObject())
                {
                    JsonProperty jp = obj;

                    if (mapNameTblToList.ContainsKey(jp.Name))
                    {
                        string listColName = mapNameTblToList[jp.Name];
                        curAdditionalData[listColName] = jp.Value;
                    }

                }

                var listItem = new ListItem
                {
                    Fields = new FieldValueSet
                    {

                        AdditionalData = curAdditionalData
                    }
                };

                var t = await graphClient.Sites[siteGuid].Lists[listNameTarg].Items.Request().AddResponseAsync(listItem);

                //Console.WriteLine($"{listItem.Id}:{t.StatusCode}");

                // Update SharePoint every maxItems record
                if (cnt % _maxPostItems == 0)
                {
                    Console.WriteLine($"Line Count Processed:{cnt}/{total}");
                }
                cnt++;

            }
            Console.WriteLine($"Line Count Processed:{cnt}/{total}");

            return true;
        }
    }

}
