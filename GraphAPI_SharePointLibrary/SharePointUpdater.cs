﻿using Azure.Identity;
using Microsoft.Graph;

namespace GraphAPI_SharePointLibrary
{
    public class SharePointUpdater
    {

        string[] scopes = new string[] { "user.read", "Sites.Read.All", "Sites.ReadWrite.All" }; //, 
        const string clientID = "5cce398a-0e10-4fd1-ae18-42305c087375";
        const string tenant = "sandc.com";
        const string redirectUrl = "msal5cce398a-0e10-4fd1-ae18-42305c087375://auth";
        GraphServiceClient graphClient = null;

        InteractiveBrowserCredentialOptions interactiveBrowserCredentialOptions;

        public SharePointUpdater()
        {
            interactiveBrowserCredentialOptions = new InteractiveBrowserCredentialOptions()
            {
                ClientId = clientID,
                RedirectUri = new System.Uri(redirectUrl)
            };
            InteractiveBrowserCredential interactiveBrowserCredential = new InteractiveBrowserCredential(interactiveBrowserCredentialOptions);


        }

        GraphServiceClient graphClient = new GraphServiceClient(interactiveBrowserCredential, scopes); // you can pass the TokenCredential directly to the GraphServiceClient
                                                                                                       //24c9b7fc-4f50-4296-8924-ce2f606f03b9
        string siteGuid = "1f066c80-d38b-4443-aea6-b04dd5e21ce2,a03ee96f-3553-47c2-8fdc-3e1d55cb6bac";
    }
}