﻿using System.Collections.Generic;
using System.Reflection;

namespace CommonChartOfAccountsMetadata.MetadataClasses
{
    /// <summary>
    /// Used to generate metadata from class properties.
    /// The metadata is used to dynamically create SharePoint list definitions
    /// </summary>
    /// <typeparam name="T">The target class to retrieve metadata from</typeparam>
    public class MapProps<T> where T : class
    {
        /// <summary>
        /// Returns a dictionary of field name, datatype to the caller.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GenerateFieldInfo( )
        {
            Dictionary<string, string> fieldDefs = new Dictionary<string, string>();
            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                string name = prop.Name;
                string DataType = prop.PropertyType.Name;
                if (DataType == "String")
                    DataType = "Text";
                fieldDefs.Add(name, DataType);
            }
            return fieldDefs;
        }

    }
}