﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonChartOfAccountsMetadata.MetadataClasses
{
    /// <summary>
    /// Used to hold the metadata information for the table as used 
    /// by SharePoint and for the columns used within the table
    /// </summary>
    public class CustomMetaDataForSharePointList
    {
        /// <summary>
        /// The name of the class, defined in ViewDefinitions, 
        /// that is the data source of the List
        /// </summary>
        public string className { get; set; }
        /// <summary>
        /// The name used for the List
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Indicates if the data in this table is US Only
        /// </summary>
        public bool US_Only { get; set; }

        public List<ListColumnType> ColumnDefinitions { get; set; } = new List<ListColumnType>();

        public Type baseClass { get; set; }

    }
}
