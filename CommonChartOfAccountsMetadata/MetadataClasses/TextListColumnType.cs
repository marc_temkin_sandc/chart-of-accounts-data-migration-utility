﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonChartOfAccountsMetadata.MetadataClasses
{
    public class TextListColumnType : ListColumnType
    {
        public int MaxLength { get; set; }
        public TextListColumnType(int maxLength)
        {
            MaxLength = maxLength;
            SharePointType = "Text";
        }
        public override string ToString() => $"{SharePointColumnName}, {SharePointType}({MaxLength})";
    }

    public class CalculatedListColumnType : ListColumnType
    {
        public string Expression { get; set; }
        public CalculatedListColumnType(string prm_expression)
        {
            Expression = prm_expression;
            SharePointType = "Calculated";
            UseCalculatedValue = true;
        }
        public override string ToString() => $"{SharePointColumnName}, {SharePointType}({Expression})";
    }
}
