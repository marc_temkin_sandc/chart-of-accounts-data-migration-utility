﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonChartOfAccountsMetadata.MetadataClasses
{
    /// <summary>
    /// Base class that describes a column definition in Share Point
    /// </summary>
    public class ListColumnType
    {
        public string SharePointColumnName { get; set; }
        public string ViewColumnName { get; set; }
        public string Description { get; set; }
        public string SharePointType { get; set; }
        public bool UseCalculatedValue { get; set; }
        public bool IsRequired { get; set; }
        public bool HasUniqueValues { get; set; }
        public bool IsLookupOnly { get; set; }
        public override string ToString() => $"{SharePointColumnName}, {SharePointType}";
        
    }
}
