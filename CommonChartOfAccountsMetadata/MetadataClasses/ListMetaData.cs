﻿using CommonChartOfAccountsMetadata.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommonChartOfAccountsMetadata.MetadataClasses
{
    public class ListMetaData
    {
        /// <summary>
        /// Creates a list containing the column type definitions
        /// need for a SharePoint list column.
        /// </summary>
        /// <returns>The list of column type definitions</returns>
        public static CustomMetaDataForSharePointList DefineViewMetaData<T>() where T: class
        {
            // The return data structure
            CustomMetaDataForSharePointList def = new CustomMetaDataForSharePointList();

            // Used to initialize the list in the SPListDefinition
            List<ListColumnType> colTypes = new List<ListColumnType>();

            // Get the type of the class used as the generic parameter
            // This is used to get the Display Name associated with the 
            // class and whether the data in this list is US-only.
            Type t = typeof(T);
            ViewAttribute viewAttr = (ViewAttribute)Attribute.GetCustomAttribute(t, typeof(ViewAttribute));
            def.className = t.Name;
            def.DisplayName = viewAttr.viewName;
            def.US_Only = viewAttr.isUS_DataOnly;

            PropertyInfo[] props = t.GetProperties();
            System.Attribute[] attrs = System.Attribute.GetCustomAttributes(t);  // Reflection.  

            foreach (PropertyInfo prop in props.Where(cond => cond.CustomAttributes != null))
            {

                ViewColumnAttribute attr = (ViewColumnAttribute)Attribute.GetCustomAttribute(prop, typeof(ViewColumnAttribute));
                if (attr != null && attr.needInList)
                {
                    ListColumnType colType = null;
                    switch (prop.PropertyType.Name)
                    {
                        case "String":
                            colType = new TextListColumnType(attr.maxLen);

                            if (attr.dataTypeConverter == "Calculation")
                            {
                                colType = new CalculatedListColumnType(
                                    attr.expression);
                            }
                            break;
                        
                        default:
                            colType = new TextListColumnType(12);
                            break;
                    }
                    // Save the name of the property (column) of the view
                    colType.ViewColumnName = prop.Name;
                    // Save the headerName from the ViewColumn attribute that decorates
                    // the property
                    colType.SharePointColumnName = attr.headerName;
                    colType.Description =attr.headerName;
                    colType.IsLookupOnly = attr.IsLookupOnly;
                    colTypes.Add(colType);
                    
                }

            }
            def.ColumnDefinitions = colTypes;

            def.baseClass = t.BaseType;
            return def;
        }
    }
}
