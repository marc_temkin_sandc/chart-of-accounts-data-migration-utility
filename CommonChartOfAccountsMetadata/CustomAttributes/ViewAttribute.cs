﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonChartOfAccountsMetadata.CustomAttributes
{
    /// <summary>
    /// Name to add to the View Name
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class ViewAttribute : System.Attribute
    {
        public string viewName;
        public bool isUS_DataOnly = false;

        public ViewAttribute(string prm_viewName, bool prm_isUS_DataOnly = false)
        {
            viewName = prm_viewName;
            isUS_DataOnly = prm_isUS_DataOnly;
        }
    }
}
