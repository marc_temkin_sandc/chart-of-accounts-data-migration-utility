﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonChartOfAccountsMetadata.CustomAttributes
{
    /// <summary>
    /// Attribute class used to decorate view columns to 
    /// attach headername, visibility, whether it is part 
    /// of an expression, if it should be converted to 
    /// a different data type.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class ViewColumnAttribute : System.Attribute
    {
        /// <summary>
        /// Display Name and used as the field name in Power Apps
        /// </summary>
        public string headerName;

        /// <summary>
        /// An expression rather than a direct table/view name reference
        /// </summary>
        public string expression = String.Empty;

        /// <summary>
        /// The name of the converter needed when the value type is not a string
        /// </summary>
        public string dataTypeConverter = String.Empty;

        /// <summary>
        /// True if the field in the view needed in the list. Defaults to false.
        /// </summary>
        public bool needInList = false;

        /// <summary>
        /// Put into description that this field is for Lookup purposes rather than display when
        /// the value is true.
        /// </summary>
        public bool IsLookupOnly = false;

        /// <summary>
        /// The max length needed when the data type is a string
        /// </summary>
        public int maxLen;

        /// <summary>
        /// Constructor to initialize all fields
        /// </summary>
        /// <param name="prm_headerName">The name of the field</param>
        /// <param name="prm_needInList">Does it belong in the list</param>
        /// <param name="prm_expression">An expression if that applies</param>
        /// <param name="prm_dataTypeConverter">A conversion function name</param>
        /// <param name="prm_maxLen">Max Length if this is a string</param>
        /// <param name="prm_IsLookupOnly">True if the field is used as a lookup and not displayed.</param>
        public ViewColumnAttribute(string prm_headerName, bool prm_needInList = false, 
            bool prm_IsLookupOnly = false, 
            string prm_expression = "", string prm_dataTypeConverter = "", int prm_maxLen=0)
        {
            headerName = prm_headerName;
            expression = prm_expression;
            dataTypeConverter = prm_dataTypeConverter;
            needInList = prm_needInList;
            maxLen = prm_maxLen;
            IsLookupOnly = prm_IsLookupOnly;
        }

        public override string ToString() => $"{headerName},{needInList}";
    }
}
