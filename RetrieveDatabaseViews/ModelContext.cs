﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using RetrieveDatabaseViews.ViewDefinitions;

namespace RetrieveDatabaseViews
{
    public partial class ModelContext : DbContext
    {
        public ModelContext()
        {
        }

        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {

        }


        public virtual DbSet<AccountsByDeptView> CoaAccountsByDeptViews { get; set; }
        public virtual DbSet<AccountsView> CoaAccountsViews { get; set; }
        public virtual DbSet<DepartmentView> CoaDepartmentViews { get; set; }
        public virtual DbSet<DeptByAccountView> CoaDeptByAccountViews { get; set; }
        public virtual DbSet<AccountsByAccountType> CoaAccountsByAccountType { get; set; }
        public virtual DbSet<AccountsByAccountTypeFrg> CoaAccountsByAccountTypeFrg { get; set; }

        public virtual DbSet<SearchView> CoaSearchView { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseOracle("User Id=APPS;Password=test1;Data Source=chi-texa-scan.sandc.ws:1521/R12G;");
                optionsBuilder.UseOracle("User Id=GENERAL;Password=read;Data Source=oci-pexa-scan.sandc.ws:1521/PRD3;");
            }
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("APPS");


            modelBuilder.Entity<AccountsByDeptView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("COA_ACCOUNTSBYDEPTVIEW");

                entity.Property(e => e.Account)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT");

                entity.Property(e => e.AccountAlias)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_ALIAS");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_NAME");

                entity.Property(e => e.Company)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("COMPANY");

                entity.Property(e => e.Department)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("DEPARTMENT");


                entity.Property(e => e.DeptName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("DEPT_NAME");

                entity.Property(e => e.Taxcode)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("TAXCODE");
            });

            modelBuilder.Entity<AccountsView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("COA_ACCOUNTSVIEW");

                entity.Property(e => e.Company)
                 .HasMaxLength(25)
                 .IsUnicode(false)
                 .HasColumnName("COMPANY");

                entity.Property(e => e.Account)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_NAME");

                entity.Property(e => e.CompositionRemarks)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("COMPOSITION_REMARKS");
            });
            modelBuilder.Entity<DepartmentView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("COA_DEPARTMENTVIEW");

                entity.Property(e => e.IS_US)
                    .HasColumnName("IS_US");

                entity.Property(e => e.Department)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("DEPARTMENT");

                entity.Property(e => e.DeptName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("DEPT_NAME");

                entity.Property(e => e.Company)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("COMPANY");
            });
            modelBuilder.Entity<DeptByAccountView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("COA_DEPTBYACCOUNTVIEW");

                entity.Property(e => e.Account)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT");

                entity.Property(e => e.Company)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("COMPANY");

                entity.Property(e => e.Department)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("DEPARTMENT");


                entity.Property(e => e.DeptName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("DEPT_NAME");
            });
            modelBuilder.Entity<SearchView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("GL_LOOKUP_V2");

                entity.Property(e => e.Company)
                   .HasMaxLength(25)
                   .IsUnicode(false)
                   .HasColumnName("COMPANY");

                entity.Property(e => e.Department)
                                  .HasMaxLength(25)
                                  .IsUnicode(false)
                                  .HasColumnName("DEPARTMENT");


                entity.Property(e => e.Account)
                                 .HasMaxLength(25)
                                 .IsUnicode(false)
                                 .HasColumnName("ACCOUNT");

                entity.Property(e => e.AccountAlias)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_ALIAS");

                entity.Property(e => e.DeptName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("DEPT_NAME");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_NAME");

                entity.Property(e => e.StartDateActive)
                    .HasColumnName("START_DATE_ACTIVE");


                entity.Property(e => e.BaseVariableCode)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("BASE_VARIABLE_CODE");

                entity.Property(e => e.Taxcode)
                                    .HasMaxLength(150)
                                    .IsUnicode(false)
                                    .HasColumnName("TAX_CODE");


                entity.Property(e => e.CompositionRemarks)
                                    .HasMaxLength(240)
                                    .IsUnicode(false)
                                    .HasColumnName("COMPOSITION_REMARKS");


            });
            modelBuilder.Entity<AccountsByAccountType>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("COA_ACCOUNTSBYACCOUNTTYPE_US");

                entity.Property(e => e.Account)
                                 .HasMaxLength(25)
                                 .IsUnicode(false)
                                 .HasColumnName("ACCOUNT");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_NAME");

                entity.Property(e => e.ShowAccountType)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("SHOW_ACCOUNT_TYPE");

                entity.Property(e => e.AccountType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_TYPE");

            });

            modelBuilder.Entity<AccountsByAccountTypeFrg>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("COA_ACCOUNTSBYACCOUNTTYPE_FRG");
                
                entity.Property(e => e.Company)
                .HasMaxLength(25)
                .IsUnicode(false)
                .HasColumnName("COMPANY");
                
                entity.Property(e => e.Account)
                                 .HasMaxLength(25)
                                 .IsUnicode(false)
                                 .HasColumnName("ACCOUNT");

                entity.Property(e => e.AccountName)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_NAME");

                entity.Property(e => e.ShowAccountType)
                    .HasMaxLength(240)
                    .IsUnicode(false)
                    .HasColumnName("SHOW_ACCOUNT_TYPE");

                entity.Property(e => e.AccountType)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("ACCOUNT_TYPE");
                
            });
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


    }
}
