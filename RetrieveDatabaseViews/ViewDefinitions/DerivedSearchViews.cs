﻿using CommonChartOfAccountsMetadata.CustomAttributes;
using System;

/// <summary>
/// File contains four views to contain different filters on
/// the GL_Lookup_V2 view
/// </summary>
namespace RetrieveDatabaseViews.ViewDefinitions
{
    

    [View(prm_viewName: "GL_Lookup_US", prm_isUS_DataOnly: true)]
    public class SearchViewUs : SearchView, ICustomSearchFilter<SearchView>
    {
        public Func<SearchView, bool> whereFilter() => (rec) => rec.Company == "01";
    }
    [View(prm_viewName: "GL_Lookup_USWithAlias", prm_isUS_DataOnly: false)]
    public class SearchViewUsWithAlias : SearchView, ICustomSearchFilter<SearchView>
    {
        public Func<SearchView, bool> whereFilter() => 
            (rec) => rec.Company == "01" && !String.IsNullOrWhiteSpace(rec.AccountAlias);
    }
    [View(prm_viewName: "GL_Lookup_NonUS", prm_isUS_DataOnly: true)]
    public class SearchViewNonUs : SearchView, ICustomSearchFilter<SearchView>
    {
        public Func<SearchView, bool> whereFilter() => (rec) => rec.Company != "01";
    }
    [View(prm_viewName: "GL_Lookup_NonUSWithAlias", prm_isUS_DataOnly: false)]
    public class SearchViewNonUsWithAlias : SearchView, ICustomSearchFilter<SearchView>
    {
        public Func<SearchView, bool> whereFilter() =>
            (rec) => rec.Company != "01" && !String.IsNullOrWhiteSpace(rec.AccountAlias);

    }

}
