﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RetrieveDatabaseViews.ViewDefinitions
{
    /// <summary>
    /// Used so that a class-specific filter can be associated with an 
    /// view definition.
    /// </summary>
    /// <typeparam name="T">The class that methods and properties of this class will use as parameter type</typeparam>
    public interface ICustomViewFilter<T> where T : class
    {
        /// <summary>
        /// Dynamic condition used for a where clause for a class of the type of the generic parameter.
        /// </summary>
        /// <returns>A condition used when querying the data source</returns>
        Func<T, bool> whereFilter();
    }

    public interface ICustomSearchFilter<T> where T : class
    {
        /// <summary>
        /// Dynamic condition used for a where clause for a class of the type of the generic parameter.
        /// </summary>
        /// <returns>A condition used when querying the data source</returns>
        Func<T, bool> whereFilter();
    }
}
