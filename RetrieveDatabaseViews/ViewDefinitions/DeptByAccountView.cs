﻿using CommonChartOfAccountsMetadata.CustomAttributes;

namespace RetrieveDatabaseViews.ViewDefinitions
{
    public class DeptByAccountView
    {
        [ViewColumn("COMPANY", prm_needInList: true,
            prm_maxLen: 25)]
        public string Company { get; set; }
        [ViewColumn("DEPARTMENT", prm_needInList: true,
            prm_maxLen: 25)]
        public string Department { get; set; }
        [ViewColumn("ACCOUNT", prm_needInList: true, prm_maxLen: 25)]
        public string Account { get; set; }

        [ViewColumn("DEPT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string DeptName { get; set; }
       
    }
}
