﻿using CommonChartOfAccountsMetadata.CustomAttributes;

namespace RetrieveDatabaseViews.ViewDefinitions
{
    
    public class AccountsView
    {
        [ViewColumn("COMPANY", prm_needInList: true, prm_IsLookupOnly: true,
           prm_maxLen: 25)]
        public string Company { get; set; }
        [ViewColumn("ACCOUNT", prm_needInList: true, prm_maxLen: 25)]
        public string Account { get; set; }
        [ViewColumn("ACCOUNT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string AccountName { get; set; }
        [ViewColumn("COMPOSITION_REMARKS", prm_needInList: true, prm_maxLen: 240)]
        public string CompositionRemarks { get; set; }
    }
}
