﻿using CommonChartOfAccountsMetadata.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetrieveDatabaseViews.ViewDefinitions
{
    
    public class SearchView
    {
        // Filters
        [ViewColumn("COMPANY", prm_needInList: true, prm_IsLookupOnly: true,
            prm_maxLen: 25)]
        public string Company { get; set; }

        // Used in the DEPT_ACCT expression
        [ViewColumn("DEPARTMENT", prm_needInList: true, prm_maxLen: 25)]
        public string Department { get; set; }

        /// <summary>
        /// ACCOUNT is used as a lookup for another form
        /// </summary>
        [ViewColumn("ACCOUNT", prm_needInList: true, prm_maxLen: 25)]
        public string Account { get; set; }

        // Not needed by form or as a lookup
        [ViewColumn("DEPT_NAME", prm_needInList: true)]
        public string DeptName { get; set; }
      
        // Fields need in form
        [ViewColumn("TAX_CODE", prm_needInList: true, prm_maxLen: 150)]
        public string Taxcode { get; set; }
        [ViewColumn("BASE_VARIABLE_CODE", prm_needInList: true, prm_maxLen: 150)]
        public string BaseVariableCode { get; set; }
        [ViewColumn("ACCOUNT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string AccountName { get; set; }
        [ViewColumn("ACCOUNT_ALIAS", prm_needInList: true, prm_maxLen: 40)]
        public string AccountAlias { get; set; }

        [ViewColumn("COMPOSITION_REMARKS", prm_needInList: true, prm_maxLen: 240)]
        public string CompositionRemarks { get; set; }

        [ViewColumn("START_DATE_ACTIVE", prm_needInList: true, prm_dataTypeConverter:"DateTime")]
        public DateTime StartDateActive { get; set; }
    }

}
