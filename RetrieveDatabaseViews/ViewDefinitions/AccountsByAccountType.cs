﻿using CommonChartOfAccountsMetadata.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetrieveDatabaseViews.ViewDefinitions
{
    
    public class AccountsByAccountType
    {
        /*
            ACCOUNT	1	VARCHAR2 (25 Byte)	Y
            ACCOUNT_NAME	2	VARCHAR2 (240 Byte)	Y
            SHOW_ACCOUNT_TYPE	3	VARCHAR2 (240 Byte)	Y
            ACCOUNT_TYPE	4	VARCHAR2 (1 Byte)	N 
         */

        /// <summary>
        /// ACCOUNT is used as a lookup for another form
        /// </summary>
        [ViewColumn("ACCOUNT", prm_needInList: true, prm_maxLen: 25)]
        public string Account { get; set; }

        [ViewColumn("ACCOUNT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string AccountName { get; set; }

        [ViewColumn("SHOW_ACCOUNT_TYPE", prm_needInList: true, prm_maxLen: 240)]
        public string ShowAccountType { get; set; }

        [ViewColumn("ACCOUNT_TYPE", prm_needInList: true, prm_maxLen: 1)]
        public string AccountType { get; set; }
    }

    public class AccountsByAccountTypeFrg 
    {
        // Filters
        [ViewColumn("COMPANY", prm_needInList: true, prm_IsLookupOnly: true,
            prm_maxLen: 25)]
        public string Company { get; set; }

        /// <summary>
        /// ACCOUNT is used as a lookup for another form
        /// </summary>
        [ViewColumn("ACCOUNT", prm_needInList: true, prm_maxLen: 25)]
        public string Account { get; set; }

        [ViewColumn("ACCOUNT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string AccountName { get; set; }

        [ViewColumn("SHOW_ACCOUNT_TYPE", prm_needInList: true, prm_maxLen: 240)]
        public string ShowAccountType { get; set; }

        [ViewColumn("ACCOUNT_TYPE", prm_needInList: true, prm_maxLen: 1)]
        public string AccountType { get; set; }

    }
}
