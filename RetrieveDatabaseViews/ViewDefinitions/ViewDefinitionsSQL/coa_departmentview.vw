DROP VIEW APPS.COA_DEPARTMENTVIEW;

/* Formatted on 8/19/2021 4:43:51 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW APPS.COA_DEPARTMENTVIEW
(
   IS_US,
   DEPARTMENT,
   DEPT_NAME,
   COMPANY
)
AS
     SELECT IS_US,
            DEPARTMENT,
            DEPT_NAME,
            COMPANY
       FROM (  SELECT UNIQUE                                    -- US Entities
                            1 AS IS_US,
                             GLCOMBS.SEGMENT2 AS Department,
                             VL_1.DESCRIPTION AS Dept_Name,
                             GLCOMBS.SEGMENT1 AS COMPANY
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
                         ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
                         ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
                WHERE     SETS_1.FLEX_VALUE_SET_NAME = 'Department'
                      AND VL_1.DESCRIPTION IS NOT NULL
                      AND GLCOMBS.SEGMENT1 = '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1, GLCOMBS.SEGMENT2, VL_1.DESCRIPTION
             UNION ALL
               SELECT UNIQUE                                    -- US Entities
                            0 AS IS_US,
                             GLCOMBS.SEGMENT2 AS Department,
                             VL_1.DESCRIPTION AS Dept_Name,
                             GLCOMBS.SEGMENT1 AS COMPANY
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
                         ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
                         ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
                WHERE     SETS_1.FLEX_VALUE_SET_NAME = 'SANDC_DEPARTMENT'
                      AND GLCOMBS.SEGMENT1 <> '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND VL_1.DESCRIPTION IS NOT NULL
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1, GLCOMBS.SEGMENT2, VL_1.DESCRIPTION)
   ORDER BY IS_US DESC, DEPT_NAME, DEPARTMENT;


GRANT SELECT ON APPS.COA_DEPARTMENTVIEW TO GL_LOOKUP;
