DROP VIEW APPS.GL_LOOKUPS;

/* Formatted on 8/19/2021 4:46:16 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW APPS.GL_LOOKUPS
(
   LOOKUP_TYPE,
   LOOKUP_CODE,
   MEANING,
   DESCRIPTION,
   ENABLED_FLAG,
   START_DATE_ACTIVE,
   END_DATE_ACTIVE
)
AS
   SELECT lookup_type LOOKUP_TYPE,
          lookup_code LOOKUP_CODE,
          meaning MEANING,
          description DESCRIPTION,
          enabled_flag ENABLED_FLAG,
          start_date_active START_DATE_ACTIVE,
          end_date_active END_DATE_ACTIVE
     FROM fnd_lookup_values LV
    WHERE     language = USERENV ('LANG')
          AND view_application_id = 101
          AND SECURITY_GROUP_ID = 0;
