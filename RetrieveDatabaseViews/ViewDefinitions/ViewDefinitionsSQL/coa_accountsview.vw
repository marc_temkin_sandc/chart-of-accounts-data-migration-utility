DROP VIEW APPS.COA_ACCOUNTSVIEW;

/* Formatted on 8/19/2021 4:43:50 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW APPS.COA_ACCOUNTSVIEW
(
   COMPANY,
   ACCOUNT,
   ACCOUNT_NAME,
   COMPOSITION_REMARKS
)
AS
     SELECT COMPANY,
            ACCOUNT,
            ACCOUNT_NAME,
            CompRemarks AS COMPOSITION_REMARKS
       FROM (  SELECT UNIQUE                                    -- US Entities
                            GLCOMBS.SEGMENT1 AS Company,
                             GLCOMBS.SEGMENT3 AS Account,
                             VL_2.DESCRIPTION AS Account_Name,
                             VL_2.ATTRIBUTE3 CompRemarks
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
                         ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
                         ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
                WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'Account'
                      AND GLCOMBS.SEGMENT1 = '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1,
                      GLCOMBS.SEGMENT3,
                      VL_2.DESCRIPTION,
                      VL_2.ATTRIBUTE3
             UNION ALL
               SELECT UNIQUE                                 --Non-US Entities
                            GLCOMBS.SEGMENT1 AS Company,
                             GLCOMBS.SEGMENT3 AS Account,
                             VL_2.DESCRIPTION AS Account_Name,
                             VL_2.ATTRIBUTE3 CompRemarks
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
                         ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
                         ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
                WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'SANDC_ACCOUNT'
                      AND GLCOMBS.SEGMENT1 != '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1,
                      GLCOMBS.SEGMENT3,
                      VL_2.DESCRIPTION,
                      VL_2.ATTRIBUTE3)
   ORDER BY account;


GRANT SELECT ON APPS.COA_ACCOUNTSVIEW TO GL_LOOKUP;
