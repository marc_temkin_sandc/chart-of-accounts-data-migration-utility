DROP VIEW APPS.COA_ACCOUNTSBYACCOUNTTYPE_FRG;

/* Formatted on 8/19/2021 4:43:46 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW APPS.COA_ACCOUNTSBYACCOUNTTYPE_FRG
(
   COMPANY,
   ACCOUNT,
   ACCOUNT_NAME,
   SHOW_ACCOUNT_TYPE,
   ACCOUNT_TYPE
)
AS
     SELECT GLCOMBS.SEGMENT1 AS COMPANY,
            Segment3 AS Account,
            VL_2.DESCRIPTION AS Account_Name,
            SHOW_ACCOUNT_TYPE,
            ACCOUNT_TYPE
       FROM gl_code_combinations_v GLCOMBS
            INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
               ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
            INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
               ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
            INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
               ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
            INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
               ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
      WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'SANDC_ACCOUNT'
            AND GLCOMBS.SEGMENT1 != '01'
            AND GLCOMBS.ENABLED_FLAG = 'Y'
            AND GLCOMBS.END_DATE_ACTIVE IS NULL
            AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                 OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
            AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
            AND CHART_OF_ACCOUNTS_ID = 50191
            AND TEMPLATE_ID IS NULL
   GROUP BY GLCOMBS.SEGMENT1,
            Segment3,
            VL_2.DESCRIPTION,
            SHOW_ACCOUNT_TYPE,
            ACCOUNT_TYPE
   ORDER BY ACCOUNT;


GRANT SELECT ON APPS.COA_ACCOUNTSBYACCOUNTTYPE_FRG TO GL_LOOKUP;
