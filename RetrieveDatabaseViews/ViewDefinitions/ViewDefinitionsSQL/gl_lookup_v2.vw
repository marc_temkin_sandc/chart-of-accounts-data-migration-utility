DROP VIEW APPS.GL_LOOKUP_V2;

/* Formatted on 8/19/2021 4:46:15 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW APPS.GL_LOOKUP_V2
(
   COMPANY,
   DEPARTMENT,
   ACCOUNT,
   DEPT_NAME,
   ACCOUNT_NAME,
   START_DATE_ACTIVE,
   BASE_VARIABLE_CODE,
   TAX_CODE,
   COMPOSITION_REMARKS,
   ACCOUNT_ALIAS
)
AS
     SELECT COMPANY,
            DEPARTMENT,
            ACCOUNT,
            DEPT_NAME,
            ACCOUNT_NAME,
            START_DATE_ACTIVE,
            BASEVARIABLE AS BASE_VARIABLE_CODE,
            TaxCode AS TAX_CODE,
            CompRemarks AS COMPOSITION_REMARKS,
            ACCOUNT_ALIAS
       FROM (  SELECT UNIQUE                                    -- US Entities
                            GLCOMBS.SEGMENT1 AS Company,
                             GLCOMBS.SEGMENT2 AS Department,
                             GLCOMBS.SEGMENT3 AS Account,
                             GLCOMBS.SEGMENT4 AS Future,
                             VL_1.DESCRIPTION AS Dept_Name,
                             VL_2.DESCRIPTION AS Account_Name,
                             GLCOMBS.ENABLED_FLAG,
                             GLCOMBS.START_DATE_ACTIVE,
                             GLCOMBS.END_DATE_ACTIVE,
                             GLCOMBS.ATTRIBUTE1 BaseVariable,
                             GLCOMBS.ATTRIBUTE3 TaxCode,
                             VL_2.ATTRIBUTE3 CompRemarks,
                             MTL.Segment1 AS ACCOUNT_ALIAS
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
                         ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
                         ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
                         ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
                         ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
                      LEFT OUTER JOIN INV.MTL_GENERIC_DISPOSITIONS MTL
                         ON MTL.distribution_account = code_combination_id
                WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'Account'
                      AND SETS_1.FLEX_VALUE_SET_NAME = 'Department'
                      AND GLCOMBS.SEGMENT1 = '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1,
                      GLCOMBS.SEGMENT2,
                      GLCOMBS.SEGMENT3,
                      GLCOMBS.SEGMENT4,
                      VL_1.DESCRIPTION,
                      VL_2.DESCRIPTION,
                      GLCOMBS.ENABLED_FLAG,
                      GLCOMBS.START_DATE_ACTIVE,
                      GLCOMBS.END_DATE_ACTIVE,
                      GLCOMBS.ATTRIBUTE1,
                      GLCOMBS.ATTRIBUTE2,
                      GLCOMBS.ATTRIBUTE3,
                      VL_2.ATTRIBUTE3,
                      MTL.Segment1
             UNION ALL                                      -- non US entities
               SELECT GLCOMBS.SEGMENT1 AS Company,
                      GLCOMBS.SEGMENT2 AS Department,
                      GLCOMBS.SEGMENT3 AS Account,
                      GLCOMBS.SEGMENT4 AS Future,
                      VL_1.DESCRIPTION AS Dept_Name,
                      VL_2.DESCRIPTION AS Account_Name,
                      GLCOMBS.ENABLED_FLAG,
                      GLCOMBS.START_DATE_ACTIVE,
                      GLCOMBS.END_DATE_ACTIVE,
                      GLCOMBS.ATTRIBUTE1 BaseVariable,
                      GLCOMBS.ATTRIBUTE3 TaxCode,
                      VL_2.ATTRIBUTE3 CompRemarks,
                      MTL.Segment1 AS ACCOUNT_ALIAS
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
                         ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
                         ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
                         ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
                         ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
                      LEFT OUTER JOIN INV.MTL_GENERIC_DISPOSITIONS MTL
                         ON MTL.distribution_account = code_combination_id
                WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'SANDC_ACCOUNT'
                      AND SETS_1.FLEX_VALUE_SET_NAME = 'SANDC_DEPARTMENT'
                      AND GLCOMBS.SEGMENT1 <> '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1,
                      GLCOMBS.SEGMENT2,
                      GLCOMBS.SEGMENT3,
                      GLCOMBS.SEGMENT4,
                      VL_1.DESCRIPTION,
                      VL_2.DESCRIPTION,
                      GLCOMBS.ENABLED_FLAG,
                      GLCOMBS.START_DATE_ACTIVE,
                      GLCOMBS.END_DATE_ACTIVE,
                      GLCOMBS.ATTRIBUTE1,
                      GLCOMBS.ATTRIBUTE2,
                      GLCOMBS.ATTRIBUTE3,
                      VL_2.ATTRIBUTE3,
                      MTL.Segment1)
   ORDER BY COMPANY, DEPARTMENT, ACCOUNT;
