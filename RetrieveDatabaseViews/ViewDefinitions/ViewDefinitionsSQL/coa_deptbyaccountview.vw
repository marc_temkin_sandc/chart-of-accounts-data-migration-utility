DROP VIEW APPS.COA_DEPTBYACCOUNTVIEW;

/* Formatted on 8/19/2021 4:43:52 PM (QP5 v5.240.12305.39446) */
CREATE OR REPLACE FORCE VIEW APPS.COA_DEPTBYACCOUNTVIEW
(
   COMPANY,
   DEPARTMENT,
   ACCOUNT,
   DEPT_NAME,
   TAXCODE,
   ACCOUNT_ALIAS
)
AS
     SELECT COMPANY,
            DEPARTMENT,
            ACCOUNT,
            DEPT_NAME,
            TAXCODE,
            ACCOUNT_ALIAS
       FROM (  SELECT UNIQUE                                    -- US Entities
                            GLCOMBS.SEGMENT1 AS Company,
                             GLCOMBS.SEGMENT2 AS Department,
                             GLCOMBS.SEGMENT3 AS Account,
                             VL_1.DESCRIPTION AS Dept_Name,
                             GLCOMBS.ATTRIBUTE3 TAXCODE,
                             MTL.Description AS ACCOUNT_ALIAS
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
                         ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
                         ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
                         ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
                         ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
                      LEFT OUTER JOIN INV.MTL_GENERIC_DISPOSITIONS MTL
                         ON code_combination_id = MTL.distribution_account
                WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'Account'
                      AND SETS_1.FLEX_VALUE_SET_NAME = 'Department'
                      AND VL_1.DESCRIPTION IS NOT NULL
                      AND GLCOMBS.SEGMENT1 = '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1,
                      GLCOMBS.SEGMENT2,
                      GLCOMBS.SEGMENT3,
                      VL_1.DESCRIPTION,
                      GLCOMBS.ATTRIBUTE3,
                      MTL.Description
             UNION ALL
               SELECT UNIQUE                               -- Non- US Entities
                            GLCOMBS.SEGMENT1 AS Company,
                             GLCOMBS.SEGMENT2 AS Department,
                             GLCOMBS.SEGMENT3 AS Account,
                             VL_1.DESCRIPTION AS Dept_Name,
                             GLCOMBS.ATTRIBUTE3 TAXCODE,
                             MTL.Description AS ACCOUNT_ALIAS
                 FROM GL.GL_CODE_COMBINATIONS GLCOMBS
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_1
                         ON VL_1.FLEX_VALUE = GLCOMBS.SEGMENT2
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_1
                         ON VL_1.FLEX_VALUE_SET_ID = SETS_1.FLEX_VALUE_SET_ID
                      INNER JOIN APPS.FND_FLEX_VALUES_VL VL_2
                         ON VL_2.FLEX_VALUE = GLCOMBS.SEGMENT3
                      INNER JOIN APPLSYS.FND_FLEX_VALUE_SETS SETS_2
                         ON VL_2.FLEX_VALUE_SET_ID = SETS_2.FLEX_VALUE_SET_ID
                      LEFT OUTER JOIN INV.MTL_GENERIC_DISPOSITIONS MTL
                         ON code_combination_id = MTL.distribution_account
                WHERE     SETS_2.FLEX_VALUE_SET_NAME = 'SANDC_ACCOUNT'
                      AND SETS_1.FLEX_VALUE_SET_NAME = 'SANDC_DEPARTMENT'
                      AND GLCOMBS.SEGMENT1 <> '01'
                      AND GLCOMBS.ENABLED_FLAG = 'Y'
                      AND GLCOMBS.END_DATE_ACTIVE IS NULL
                      AND (   GLCOMBS.END_DATE_ACTIVE IS NULL
                           OR TRUNC (GLCOMBS.END_DATE_ACTIVE) > TRUNC (SYSDATE)) --IS NULL
                      AND GLCOMBS.start_DATE_ACTIVE IS NOT NULL
             GROUP BY GLCOMBS.SEGMENT1,
                      GLCOMBS.SEGMENT2,
                      GLCOMBS.SEGMENT3,
                      VL_1.DESCRIPTION,
                      GLCOMBS.ATTRIBUTE3,
                      MTL.Description)
   ORDER BY ACCOUNT,
            DEPT_NAME,
            DEPARTMENT,
            COMPANY;


GRANT SELECT ON APPS.COA_DEPTBYACCOUNTVIEW TO GL_LOOKUP;
