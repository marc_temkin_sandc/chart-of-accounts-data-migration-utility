﻿using CommonChartOfAccountsMetadata.CustomAttributes;

namespace RetrieveDatabaseViews.ViewDefinitions
{
    public class AccountsByDeptView
    {
        // Filters
        [ViewColumn("COMPANY", prm_needInList: true, prm_IsLookupOnly: true,
            prm_maxLen: 25)]
        public string Company { get; set; }

        // Used in the DEPT_ACCT expression
        [ViewColumn("DEPARTMENT", prm_needInList: true, prm_maxLen: 25)]
        public string Department { get; set; }

        /// <summary>
        /// ACCOUNT is used as a lookup for another form
        /// </summary>
        [ViewColumn("ACCOUNT", prm_needInList: true, prm_IsLookupOnly: true, prm_maxLen: 25)]
        public string Account { get; set; }

        // Not needed by form or as a lookup
        [ViewColumn("DEPT_NAME", prm_needInList: false)]
        public string DeptName { get; set; }
    


        // Fields need in form
        [ViewColumn("TAXCODE", prm_needInList: true, prm_maxLen: 150)]
        public string Taxcode { get; set; }
        [ViewColumn("ACCOUNT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string AccountName { get; set; }
        [ViewColumn("ACCOUNT_ALIAS", prm_needInList: true, prm_maxLen: 40)]
        public string AccountAlias { get; set; }
    }
}
