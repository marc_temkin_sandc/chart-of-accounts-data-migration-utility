﻿using CommonChartOfAccountsMetadata.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetrieveDatabaseViews.ViewDefinitions
{

    [View(prm_viewName: "Accts By Dept US", prm_isUS_DataOnly: true)]
    public class AccountsByDeptViewUS : AccountsByDeptView, 
        ICustomViewFilter<AccountsByDeptView>, IXRefView
    {
        [ViewColumn("DEPT_ACCT", prm_needInList: true, prm_IsLookupOnly:false, prm_expression: "[DEPARTMENT]&" + "-" + "&[ACCOUNT])", prm_dataTypeConverter: "Calculation")]
        public string DeptAcct { get; set; }

        public Func<AccountsByDeptView, bool> whereFilter() => (rec) => rec.Company == "01";
    }

    [View(prm_viewName: "Accts By Dept NonUS", prm_isUS_DataOnly: false)]
    public class AccountsByDeptViewNonUS : AccountsByDeptView,
     ICustomViewFilter<AccountsByDeptView>, IXRefView
    {
        [ViewColumn("DEPT_ACCT", prm_needInList: true)]
        public string DeptAcct { get; set; }

        public Func<AccountsByDeptView, bool> whereFilter() => (rec) => rec.Company != "01";

    }

    [View(prm_viewName: "Depts US", prm_isUS_DataOnly: true)]
    public partial class DepartmentViewUS : DepartmentView, ICustomViewFilter<DepartmentView>
    {
        public Func<DepartmentView, bool> whereFilter() => (rec) => rec.IS_US == 1;
        
    }

    [View(prm_viewName: "AccountsByAccountTypeUS", prm_isUS_DataOnly: true)]
    public partial class vAccountsByAccountType : AccountsByAccountType, ICustomViewFilter<AccountsByAccountType>
    {
        public Func<AccountsByAccountType, bool> whereFilter() => (rec) => true;

    }

    [View(prm_viewName: "AccountsByAccountTypeNonUS", prm_isUS_DataOnly: true)]
    public partial class vAccountsByAccountTypeFrg : AccountsByAccountTypeFrg, ICustomViewFilter<AccountsByAccountTypeFrg>
    {
        public Func<AccountsByAccountTypeFrg, bool> whereFilter() => (rec) => true;

    }

    [View(prm_viewName: "Depts Non US", prm_isUS_DataOnly: false)]
    public partial class DepartmentViewNonUS : DepartmentView, ICustomViewFilter<DepartmentView>
    {
        public Func<DepartmentView, bool> whereFilter() => (rec) => rec.IS_US == 0;
    }

    [View(prm_viewName: "Dept By Account US", prm_isUS_DataOnly: true)]
    public partial class DeptByAccountViewUS : DeptByAccountView,
        ICustomViewFilter<DeptByAccountView>, IXRefView
    {
        [ViewColumn("DEPT_ACCT", prm_needInList: true)]
        public string DeptAcct { get; set; }

        public Func<DeptByAccountView, bool> whereFilter() => (rec) => rec.Company == "01";

    }


    [View(prm_viewName: "Dept By Account Non US", prm_isUS_DataOnly: false)]
    public partial class DeptByAccountViewNonUS : DeptByAccountView,
        ICustomViewFilter<DeptByAccountView>, IXRefView
    {
        [ViewColumn("DEPT_ACCT", prm_needInList: true)]
        public string DeptAcct { get; set; }

        public Func<DeptByAccountView, bool> whereFilter() => (rec) => rec.Company != "01";
    }

    [View(prm_viewName: "Accounts US", prm_isUS_DataOnly: true)]
    public partial class AccountsViewUS : AccountsView, ICustomViewFilter<AccountsView>
    {
        public Func<AccountsView, bool> whereFilter() => (rec) => rec.Company == "01";
    }

    [View(prm_viewName: "Accounts Non US", prm_isUS_DataOnly: false)]
    public partial class AccountsViewNonUS : AccountsView, ICustomViewFilter<AccountsView>
    {
        public Func<AccountsView, bool> whereFilter() => (rec) => rec.Company != "01";
    }
}
