﻿using CommonChartOfAccountsMetadata.CustomAttributes;
namespace RetrieveDatabaseViews.ViewDefinitions
{
    
    public  class DepartmentView
    {
        [ViewColumn("IS_US", prm_needInList: false, prm_IsLookupOnly:true )]
        public int IS_US { get; set; }
        [ViewColumn("DEPARTMENT", prm_needInList: true, prm_maxLen: 25)]
        public string Department { get; set; }
        [ViewColumn("DEPT_NAME", prm_needInList: true, prm_maxLen: 240)]
        public string DeptName { get; set; }
        [ViewColumn("COMPANY", prm_needInList: true, prm_IsLookupOnly: true,
   prm_maxLen: 25)]
        public string Company { get; set; }
    }
}
